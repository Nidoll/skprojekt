﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton_Mage_Script : Enemy_Script {

	private bool ContactToPlayer;
	public bool Knockback = false;
	private Vector2 PlayerPositionV2;
	private Vector2 CurrentPosition2V;
	private float time;

	private Vector2 OldPosition;
	private int Counter = 0;
	private Animator SkeletonAnimator;

	private float ShootFrequenz = 4;
	private float PastTime;
	private GameObject SoulBall;

	private bool PlayerInRange = false;

	// Use this for initialization
	void Start () 
	{
		StartEnemy();
		EnemyDamage = 0.05f;
		HpEnemy = 4;
		SkeletonAnimator = GetComponent<Animator>();
		SoulBall = PlayerManager.instance.Seelenball;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(Vector2.Distance(Player.transform.position, transform.position) < 10)
		{
			PlayerInRange = true;
		}else{
			PlayerInRange = false;
		}

		if(PlayerInRange == true)
		{
			PlayerPositionV2 = Player.transform.position;
			CurrentPosition2V = transform.position;
		
			TestForObstacles();
			CalcDirectionToPlayer();
			transform.eulerAngles = new Vector3(0,0,0);

			if(DirectionToPlayer.y <= 0)
			{
				SkeletonAnimator.SetBool("IsWalkingForward", true);
				SkeletonAnimator.SetBool("IsWalkingBackwards", false);
			}else{
				SkeletonAnimator.SetBool("IsWalkingForward", false);
				SkeletonAnimator.SetBool("IsWalkingBackwards", true);
			}

			if(ObstaclesToPlayer == true && PathFound == false)
			{
				Pathfinding();
			}

			if(HpEnemy <= 0)
			{
				Destroy(this.gameObject);
			}

			PastTime += Time.deltaTime;

			if(PastTime >= ShootFrequenz)
			{
				float z = Mathf.Atan2((Player.transform.position.y - transform.position.y), (Player.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;
				Instantiate(SoulBall, transform.position, Quaternion.Euler(0,0,z));
				PastTime = 0;
			}

		}


	}

	void FixedUpdate()
	{

		if(PlayerInRange == true)
		{
			
			if(ObstaclesToPlayer == true && PathFound == true && ContactToPlayer == false && Knockback == false && Vector2.Distance(Player.transform.position,transform.position) > 5)
			{
				MovePath();
			}else if(ContactToPlayer == false && Knockback == false && Vector2.Distance(Player.transform.position,transform.position) > 5.5)
			{
				EnemyRigidbody.MovePosition(new Vector2(transform.position.x,transform.position.y)+DirectionToPlayer*MoveSpeed*Time.fixedDeltaTime);
			}else if(Knockback)
			{
				if(time < 0.2f)
				{
					GetComponent<Rigidbody2D>().MovePosition(CurrentPosition2V - (PlayerPositionV2-CurrentPosition2V) * Time.fixedDeltaTime * 5);
					time += Time.deltaTime;
				}else{
					Knockback = false;
					time = 0;
				}
			}else if(Vector2.Distance(Player.transform.position,transform.position) < 5)
			{
				EnemyRigidbody.MovePosition(new Vector2(transform.position.x,transform.position.y)-DirectionToPlayer*MoveSpeed*Time.fixedDeltaTime);
			}		
		}	
	}

	void OnCollisionStay2D(Collision2D coll)
	{
		/* 
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = true;
			Player.GetComponent<PlayerController>().getDamage(EnemyDamage,transform.position);
		}
		*/
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		
		if(coll.CompareTag("Waffe") && Knockback == false)
		{
			transform.GetComponentInChildren<ParticleSystem>().Play();
			GetDamage(Player.GetComponent<PlayerController>().WeaponDamage);
			Knockback = true;
		}

		if(coll.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll, GetComponent<Collider2D>());
		}

		if(coll.CompareTag("Seelenball"))
		{
			transform.GetComponentInChildren<ParticleSystem>().Play();
			GetDamage(2);
			Knockback = true;
		}

	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.transform.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll.collider, GetComponent<Collider2D>());
		}
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = false;
		}
	}
}
