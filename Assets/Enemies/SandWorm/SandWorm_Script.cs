﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SandWorm_Script : Enemy_Script {

	private bool ContactToPlayer;
	public bool Knockback = false;
	private Vector2 PlayerPositionV2;
	private Vector2 CurrentPosition2V;
	private float time;
	public float DistanceToPlayer2;
	private Vector2 OldPosition;
	private int Counter = 0;
	private Animator SandWormAnimator;

	private bool PlayerInRange = false;

	// Use this for initialization
	void Start () 
	{
		StartEnemy();
		EnemyDamage = 0.1f;
		HpEnemy = 5;
		SandWormAnimator = GetComponent<Animator>();	
	}
	
	// Update is called once per frame
	void Update () 
	{
		DistanceToPlayer2 = Vector2.Distance(Player.transform.position, transform.position);

		if(Vector2.Distance(Player.transform.position, transform.position) < 1.25f)
		{
			PlayerInRange = true;
		}else{
			PlayerInRange = false;
		}

		if(DirectionToPlayer.x < 0)
		{
			GetComponent<SpriteRenderer>().flipX = true;
		}else{
			GetComponent<SpriteRenderer>().flipX = false;
		}

		if(PlayerInRange == true)
		{
			PlayerPositionV2 = Player.transform.position;
			CurrentPosition2V = transform.position;
			SandWormAnimator.SetBool("IsAttacking",true);
			GetComponent<BoxCollider2D>().enabled = true;
		
			transform.eulerAngles = new Vector3(0,0,0);

			if(HpEnemy <= 0)
			{
				Destroy(this.gameObject);
			}
		}else{
			SandWormAnimator.SetBool("IsAttacking",false);
			GetComponent<BoxCollider2D>().enabled = false;
		}

		
	}

	void FixedUpdate()
	{

		if(PlayerInRange == true)
		{
			
			
			
					
		}	
	}

	void OnTriggerStay2D(Collider2D coll)
	{
		
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = true;
			Player.GetComponent<PlayerController>().getDamage(EnemyDamage,transform.position);
		}
		
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		
		if(coll.CompareTag("Waffe"))
		{
			transform.GetComponentInChildren<ParticleSystem>().Play();
			GetDamage(Player.GetComponent<PlayerController>().WeaponDamage);
		}

		if(coll.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll, GetComponent<Collider2D>());
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.transform.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll.collider, GetComponent<Collider2D>());
		}
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = false;
		}
	}
}
