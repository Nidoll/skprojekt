﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suicide_Gurke_Script : Enemy_Script {

	private bool ContactToPlayer;
	public bool Knockback = false;
	private Vector2 PlayerPositionV2;
	private Vector2 CurrentPosition2V;
	private float time;

	private Vector2 OldPosition;
	private int Counter = 0;
	private Animator SkeletonAnimator;

	private bool PlayerInRange = false;

	public AudioSource GurkeSource;
	public AudioClip ExploSound;
	public AudioClip ScreamSound;
	private bool SoundIsPlaying = false;

	// Use this for initialization
	void Start () 
	{
		StartEnemy();
		EnemyDamage = 0.15f;
		HpEnemy = 1;
		SkeletonAnimator = GetComponent<Animator>();
		GurkeSource = GetComponent<AudioSource>();	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Vector2.Distance(Player.transform.position, transform.position) < 10)
		{
			PlayerInRange = true;
		}else{
			PlayerInRange = false;
		}

		if(PlayerInRange == true)
		{
			PlayerPositionV2 = Player.transform.position;
			CurrentPosition2V = transform.position;
		
			TestForObstacles();
			CalcDirectionToPlayer();
			transform.eulerAngles = new Vector3(0,0,0);

			if(DirectionToPlayer.x >= 0)
			{
				GetComponent<SpriteRenderer>().flipX = true;
			}else{
				GetComponent<SpriteRenderer>().flipX = false;
			}

			if(ObstaclesToPlayer == true && PathFound == false)
			{
				Pathfinding();
			}

			if(HpEnemy <= 0)
			{
				StartCoroutine(Die());
			}
		}

		if(PlayerInRange && !SoundIsPlaying)
		{
			Debug.Log("Play");
			GurkeSource.Play();
			SoundIsPlaying = true;
		}
		
		if(!PlayerInRange && SoundIsPlaying)
		{
			Debug.Log("Stop");
			GurkeSource.Stop();
			SoundIsPlaying = false;
		}
		
	}

	void FixedUpdate()
	{

		if(PlayerInRange == true)
		{
			
			if(ObstaclesToPlayer == true && PathFound == true && ContactToPlayer == false && Knockback == false)
			{
				MovePath();
			}else if(ContactToPlayer == false && Knockback == false)
			{
				EnemyRigidbody.MovePosition(new Vector2(transform.position.x,transform.position.y)+DirectionToPlayer*MoveSpeed*Time.fixedDeltaTime);
			}else if(Knockback)
			{
				if(time < 0.2f)
				{
					GetComponent<Rigidbody2D>().MovePosition(CurrentPosition2V - (PlayerPositionV2-CurrentPosition2V) * Time.fixedDeltaTime * 5);
					time += Time.deltaTime;
				}else{
					Knockback = false;
					time = 0;
				}
			}
					
		}	
	}

	void OnCollisionStay2D(Collision2D coll)
	{
		
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = true;
			Player.GetComponent<PlayerController>().getDamage(EnemyDamage,transform.position);
			StartCoroutine(Die());
		}
		
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		
		if(coll.CompareTag("Waffe") && Knockback == false)
		{
			GurkeSource.PlayOneShot(ExploSound);
			transform.GetComponentInChildren<ParticleSystem>().Play();
			GetDamage(Player.GetComponent<PlayerController>().WeaponDamage);
			Knockback = true;
		}

		if(coll.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll, GetComponent<Collider2D>());
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.transform.CompareTag("SolidObject"))
		{
			Debug.Log("Test");
			Physics2D.IgnoreCollision(coll.collider, GetComponent<Collider2D>());
		}
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		if(coll.transform.CompareTag("Player"))
		{
			ContactToPlayer = false;
		}
	}

	IEnumerator Die()
	{
		transform.GetComponentInChildren<ParticleSystem>().Play();
		yield return new WaitForSeconds(0.2f);
		Instantiate(PlayerManager.instance.Gurke_Item, transform.position, Quaternion.identity);
		Destroy(gameObject);
		yield break;
	}

	
}
