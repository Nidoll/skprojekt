﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Tilemaps;

public class Enemy_Script : MonoBehaviour {

	public Vector3 CP;
	public float z;
	public Vector2 CCenter;
	public string direction;
	public GameObject CollPoint;
	
	
	public Vector2[] NavPoints;
	public Vector2 PlayerPosition;
	public GameObject NavPoint;
	public Vector2 CenterPoint;
	public Vector2 StartPoint;

	public bool ObstaclesToPlayer = true;
	public GameObject Player;
	public float MoveSpeed = 5;

	//---Autpopahting---

	public Vector2[] CellPoints = new Vector2[4];
	public List<Vector2> PathPoints = new List<Vector2>();
	public Vector2 OriginCellPosition;
	public Vector2 CurrentCellPosition;
	public Vector2 ChoosenCellPosition;
	public float[] DistanceToOrigin = new float[4];
	public float[] DistanceToPlayer = new float[4];
	public float[] CellScore = new float[4];
	public bool PathFound = false;
	public int LowestCellScore;
	public Tilemap Border;
	public bool CalcDone = false;
	public Vector2 OldPlayerPosition;
	public bool check = false;
	public int TimeCount = 0;
	public Vector3[] EnemyCornerPoints = new Vector3[4];
	public Vector3[] PlayerCornerPoints = new Vector3[4];
	
	
	public Vector2 DirectionToPlayer;
	public Rigidbody2D EnemyRigidbody;

	//-- Battle System --

	public float HpEnemy;
	public float EnemyDamage;



	// Use this for initialization
	void Start () 
	{
		StartEnemy();
	}
	
	public void StartEnemy()
	{
		PlayerPosition = new Vector2(PlayerManager.instance.Player.transform.position.x,PlayerManager.instance.Player.transform.position.y);
		StartPoint = transform.position;
		CenterPoint = transform.position;
		Player = PlayerManager.instance.Player;
		Border = PlayerManager.instance.BorderTilemap;
		EnemyRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{ 
		EnemyCornerPoints[0] = new Vector3(transform.position.x + 0.5f, transform.position.y + 0.5f, 0);
		EnemyCornerPoints[1] = new Vector3(transform.position.x + 0.5f, transform.position.y - 0.5f, 0);
		EnemyCornerPoints[2] = new Vector3(transform.position.x - 0.5f, transform.position.y + 0.5f, 0);
		EnemyCornerPoints[3] = new Vector3(transform.position.x - 0.5f, transform.position.y - 0.5f, 0);

		PlayerCornerPoints[0] = new Vector3(Player.transform.position.x + 0.5f, Player.transform.position.y + 0.5f, 0);
		PlayerCornerPoints[1] = new Vector3(Player.transform.position.x + 0.5f, Player.transform.position.y - 0.5f, 0);
		PlayerCornerPoints[2] = new Vector3(Player.transform.position.x - 0.5f, Player.transform.position.y + 0.5f, 0);
		PlayerCornerPoints[3] = new Vector3(Player.transform.position.x - 0.5f, Player.transform.position.y - 0.5f, 0);

		if(Input.GetKeyDown(KeyCode.O))
		{
			//PathfindingTest();
		}
		
		PlayerPosition = new Vector2(PlayerManager.instance.Player.transform.position.x,PlayerManager.instance.Player.transform.position.y);	
		if(TimeCount < 10)
		{
			TimeCount++;
		}else{
			TimeCount = 0;
			OldPlayerPosition = Player.transform.position;
		}
		CCenter = GetComponent<BoxCollider2D>().bounds.center;
		CollPoint.transform.position = CP;	

		Navigation(PlayerPosition);
		NavPoint.transform.position = CenterPoint;

		//Debug.DrawRay(transform.position,(Player.transform.position-transform.position).normalized*(Vector3.Distance(Player.transform.position,transform.position)),Color.red);

		if(Physics2D.Raycast(EnemyCornerPoints[0],(PlayerCornerPoints[0]-EnemyCornerPoints[0]).normalized,(Vector3.Distance(PlayerCornerPoints[0],EnemyCornerPoints[0])),LayerMask.GetMask("Wall"))
			|| Physics2D.Raycast(EnemyCornerPoints[1],(PlayerCornerPoints[1]-EnemyCornerPoints[1]).normalized,(Vector3.Distance(PlayerCornerPoints[1],EnemyCornerPoints[1])),LayerMask.GetMask("Wall"))
			|| Physics2D.Raycast(EnemyCornerPoints[2],(PlayerCornerPoints[2]-EnemyCornerPoints[2]).normalized,(Vector3.Distance(PlayerCornerPoints[2],EnemyCornerPoints[2])),LayerMask.GetMask("Wall"))
			|| Physics2D.Raycast(EnemyCornerPoints[3],(PlayerCornerPoints[3]-EnemyCornerPoints[3]).normalized,(Vector3.Distance(PlayerCornerPoints[3],EnemyCornerPoints[3])),LayerMask.GetMask("Wall"))
			)
		{
			ObstaclesToPlayer = true;
			//Pathfinding();
		}else{
			ObstaclesToPlayer = false;
			PathFound = false;
			//PathPoints.Clear();
		}

		if(ObstaclesToPlayer == true && PathFound == false)
		{
			Pathfinding();
		}
	}

	void FixedUpdate()
	{
		if(ObstaclesToPlayer == false)
		{
			GetComponent<Rigidbody2D>().MovePosition(transform.position+(Player.transform.position-transform.position).normalized*Time.fixedDeltaTime*MoveSpeed);
		}else{
			MovePath();
		}

		if(PathFound == true)
		{
			//MovePath();
		}
	}

	public void CalcDirectionToPlayer()
	{
		DirectionToPlayer = (Player.transform.position-transform.position).normalized;
	}

	void OnCollisionStay2D(Collision2D coll)
	{
		
	}

	void Navigation(Vector2 PlayerPosition)
	{
		//Debug.Log(Vector2.Distance(transform.position, CenterPoint));
		//Debug.Log(CenterPoint.x - transform.position.x);

		if(CenterPoint.x - transform.position.x < -0.5)
		{
			CenterPoint.x += 1;
		}else if(CenterPoint.x - transform.position.x > 0.5)
		{
			CenterPoint.x -= 1;
		}else if(CenterPoint.y - transform.position.y < -0.5)
		{
			CenterPoint.y += 1;
		}else if(CenterPoint.y - transform.position.y > 0.5)
		{
			CenterPoint.y -= 1;
		}

	}


	public void MovePath()
	{
		if(PathPoints != null)
		{
				if(PathPoints.Count > 0)
				{
					//transform.position = Vector2.MoveTowards(transform.position,PathPoints[0],2*Time.fixedDeltaTime);
					transform.position = Vector2.MoveTowards(transform.position,PathPoints[0],5*Time.fixedDeltaTime);
				}else{
					PathFound = false;
				}
		 
			if(Vector2.Distance(transform.position,PathPoints[0]) < 0.1 && PathPoints.Count > 0)
			{
				PathPoints.Remove(PathPoints[0]);
				//PathfindingTest();
			}
		}
		
		
	}



	public void Pathfinding()
	{
		OriginCellPosition = new Vector2(Mathf.FloorToInt(transform.position.x),Mathf.FloorToInt(transform.position.y));
		if(PathPoints != null)
		{
			PathPoints.Clear();
		}
		for(int i = 0; i < 4; i++)
		{
			CellScore[i] = 0;
			CellPoints[i] = Vector2.zero;
		}
		
		//for(int x = 0; x < 100; x++)
		for(int x = 0; PathFound == false; x++)
		{	
			if(x > 100)
			{
				break;
			}

			for(int i = 0; i < 4; i++)
			{	
				if(i == 0)
				{
					CurrentCellPosition = new Vector2(OriginCellPosition.x + 1, OriginCellPosition.y);
				}else if(i == 1){
					CurrentCellPosition = new Vector2(OriginCellPosition.x, OriginCellPosition.y + 1);
				}else if(i == 2){
					CurrentCellPosition = new Vector2(OriginCellPosition.x - 1, OriginCellPosition.y);
				}else if(i == 3){
					CurrentCellPosition = new Vector2(OriginCellPosition.x, OriginCellPosition.y - 1);
				}
		
				CellPoints[i] = CurrentCellPosition;
				//Debug.Log(NavPoints.Contains(CurrentCellPosition));
				
				if(Border.GetTile(new Vector3Int(Mathf.RoundToInt(CellPoints[i].x),Mathf.RoundToInt(CellPoints[i].y),0)) == null)
				{
					DistanceToOrigin[i] = Vector2.Distance(CurrentCellPosition, OriginCellPosition);
					DistanceToPlayer[i] = Vector2.Distance(CurrentCellPosition, new Vector2(Player.transform.position.x,Player.transform.position.y));
		
					CellScore[i] = DistanceToOrigin[i] + DistanceToPlayer[i];
				}else{
					CellScore[i] = 1000;
				}

					
				if(x > 0)
				{
					if(PathPoints.Contains(CellPoints[i]))
					{
						CellScore[i] = 1000;
					}
				}


			}


			for(int i = 0; i < 4; i++)
			{
				if(CellScore.Min() == CellScore[i])
				{
					LowestCellScore = i;
				}
			}
			
			
			
			PathPoints.Add(CellPoints[LowestCellScore]);
			

			if(CellPoints[LowestCellScore] == new Vector2(Mathf.Floor(Player.transform.position.x),Mathf.Floor(Player.transform.position.y)))
			{
				Debug.Log("Path found");
				for(int i = 0; i < PathPoints.Count; i++)
				{
					PathPoints[i] += new Vector2(0.5f,0.5f);
				}
				PathFound = true;
				break;
			}

			
			
			
			OriginCellPosition = CellPoints[LowestCellScore];
			
		}

		
	}

	public void TestForObstacles()
	{

		if(Physics2D.Raycast(transform.position,(Player.transform.position-transform.position).normalized,(Vector3.Distance(Player.transform.position,transform.position)),LayerMask.GetMask("Wall")))
		{
			ObstaclesToPlayer = true;
			//Pathfinding();
		}else{
			ObstaclesToPlayer = false;
			PathFound = false;
			//PathPoints.Clear();
		}
	}

	public void GetDamage(int DamageAmount)
	{

		HpEnemy -= DamageAmount;

	}

	


}
