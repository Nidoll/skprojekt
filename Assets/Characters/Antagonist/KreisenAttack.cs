﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KreisenAttack : MonoBehaviour {

	public GameObject[] Spots = new GameObject[4];
	public List<int> RandomNumbers;
	public int RandomNumber;
	public float speed;
	public float Interwall;
	public int counter = 0;
	private GameObject Seelenball;
	private GameObject Player;
	private bool skip = false;
	private float z;
	private int m = -1;

	void Start () 
	{
		for(int i = 0; i < 4; i++)
		{
			Spots[i] = transform.Find("Spot"+i.ToString()).gameObject;
		}
		Seelenball = PlayerManager.instance.Seelenball.gameObject;
		Player = PlayerManager.instance.Player.gameObject;
	}
	
	void Update() 
	{
		transform.position = PlayerManager.instance.Player.transform.position;

		if(counter < 4)
		{
			RandomNumber = Random.Range(0,4);
			if(!RandomNumbers.Contains(RandomNumber))
			{
				RandomNumbers.Add(RandomNumber);
				counter++;
			}
		}else if(!skip)
		{
			StartCoroutine(Shoot());
			skip = true;
		}

	}

	void FixedUpdate()
	{
		transform.Rotate(-Vector3.forward * Time.fixedDeltaTime * speed);
	}

	IEnumerator Shoot()
	{
		while(m < 4)
		{
			if(m == -1)
			{
				m++;
				yield return new WaitForSeconds(3f);
			}else{
				z = Mathf.Atan2((Player.transform.position.y - transform.Find("Spot"+RandomNumbers[m]).transform.position.y), (Player.transform.position.x - transform.Find("Spot"+RandomNumbers[m]).transform.position.x)) * Mathf.Rad2Deg - 90;
				Instantiate(Seelenball, transform.Find("Spot"+RandomNumbers[m]).position, Quaternion.Euler(0,0,z));
				Destroy(transform.Find("Spot"+RandomNumbers[m]).gameObject);
				m++;
				yield return new WaitForSeconds(2f);
			}
		}
		Destroy(gameObject);
		yield break;
	}
}
