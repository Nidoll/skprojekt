﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAsmo : MonoBehaviour {


	public GameObject Asmo;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Asmo.SetActive(true);
			Destroy(gameObject);
		}
	}
}
