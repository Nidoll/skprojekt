﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shots : MonoBehaviour {

	public GameObject[] Spots = new GameObject[3];
	public GameObject Seelenball;
	private int Phase = 1;
	private float Attacks;

	private GameObject Player;
	void Start () 
	{
		Player = PlayerManager.instance.Player;
		for(int i = 0; i < 3; i++)
		{
			Spots[i] = transform.Find("Spot"+(i+1).ToString()).gameObject;
		}
		Seelenball = PlayerManager.instance.Seelenball.gameObject;
	}
	
	void Update () 
	{
		float z = Mathf.Atan2((Player.transform.position.y - transform.position.y), (Player.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;
		transform.eulerAngles = new Vector3(0, 0, z);
	}

	public void MultiShoot(int AttackAmount)
	{
		switch(AttackAmount)
		{
			case 1:
				ShootSingle();
				break;
			case 2:
				DoubleShoot();
				break;
			case 3:
				TripleShoot();
				break;
		}
	}

	public void ShootSingle()
	{
		Instantiate(Seelenball,Spots[1].transform.position,Spots[1].transform.rotation);
	}

	public void DoubleShoot()
	{
		Instantiate(Seelenball,Spots[0].transform.position,Spots[0].transform.rotation);
		Instantiate(Seelenball,Spots[2].transform.position,Spots[2].transform.rotation);
	}

	public void TripleShoot()
	{
		Instantiate(Seelenball,Spots[0].transform.position,Spots[0].transform.rotation);
		Instantiate(Seelenball,Spots[1].transform.position,Spots[1].transform.rotation);
		Instantiate(Seelenball,Spots[2].transform.position,Spots[2].transform.rotation);
	}
}
