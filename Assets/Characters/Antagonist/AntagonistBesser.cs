﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class AntagonistBesser : MonoBehaviour {

	public bool shild = true;
	private int Attack;
	public GameObject KreisAttack;
	private bool skip = false;
	public GameObject Canvas;
	public GameObject Healthbar;
	//HP----------------------
	public float MaxHP;
    public float HP;
    public float HPPercent;
	public Slider HPSlider;
	public bool Beatleball;
	
	void Start () 
	{
		HP = MaxHP;
		Canvas = PlayerManager.instance.Canvas;
		if(Beatleball == true)
		{
			Instantiate(PlayerManager.instance.TutorialBossBar,new Vector3(350,30,0),Quaternion.identity,Canvas.transform);
			HPSlider = Canvas.transform.Find("Bar(Clone)").gameObject.GetComponent<Slider>();
		}
	}
	void Update()
	{
		if(shild)
		{
			transform.Find("Schild").gameObject.SetActive(true);
		}else{
			transform.Find("Schild").gameObject.SetActive(false);
		}

		if(!skip)
		{
			StartCoroutine(RandomAttacks(1));
			skip = true;
		}
		if(Beatleball == true)
		{
			HPSlider.value = HP / 100;
		}
		if(HP <= 0)
		{
			Destroy(GameObject.Find("KreisenAttack(Clone)"));
			Destroy(Canvas.transform.Find("Bar(Clone)").gameObject);
			PlayerManager.instance.GetComponent<AudioSource>().Stop();
			PlayerManager.instance.PlayCredits();
			Destroy(gameObject);
		}
	}
	public void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Waffe") && shild == false)
		{
			//GetDamage(other.GetComponent<KristellSchwert>().damage);
			if(Beatleball == true)
			{
				HP -= 10;
			}else{
				HP = HP - HP/100*20;
			}
		}

		if(other.CompareTag("Seelenball"))
		{
			StopAllCoroutines();
			StartCoroutine(Delay(2));
			shild = false;
		}
	}
	void GetDamage(float Damage)
	{
		HP -= Damage;
	}

	public void Attacks(int RandomNumber)
	{
		switch(RandomNumber)
		{
			case 1:
				Instantiate(KreisAttack);
				break;
			case 2:
				GetComponentInChildren<Shots>().MultiShoot(2);
				break;
			case 3:
				GetComponentInChildren<Shots>().MultiShoot(1);
				break;
			case 4:
				GetComponentInChildren<Shots>().MultiShoot(1);
				break;

		}
	}

	IEnumerator RandomAttacks(int Phase)
	{
		if(Phase == 1)
		{
			for(int i = 0; i < 5; i++)
		{
			int RandomN = Random.Range(1,5);
				if(RandomN == 1)
				{
					Attacks(RandomN);
					yield return new WaitForSeconds(10);
				}else{
					Attacks(RandomN);
					yield return new WaitForSeconds(1);
				}
		}
			skip = false;
			yield break;
		}
		
	}
	IEnumerator Delay(float time)
	{
		bool times = false;
		if(times == false)
		{
			times = true;
			yield return new WaitForSeconds(time);
		}
		shild = true;
		skip = false;
		yield break;
	}
}
