﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTriggerT : MonoBehaviour {

	public GameObject Boss;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Instantiate(Boss);
			Destroy(gameObject);
		}
	}
}
