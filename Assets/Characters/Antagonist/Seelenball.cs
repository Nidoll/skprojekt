﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seelenball : MonoBehaviour {

	private Rigidbody2D rig;
	public float speed;

	public float flightTime;
	public float currentTime = 0;
	private Vector3 trans;
	private GameObject Player;

	void Start () 
	{
		rig = GetComponent<Rigidbody2D>();
		trans = transform.up;
		Player = PlayerManager.instance.Player;
	}
	
	void Update () 
	{
		currentTime += Time.deltaTime;
		if(flightTime <= currentTime)
		Destroy(gameObject);
	}

	void FixedUpdate()
	{
		rig.MovePosition(transform.position + transform.up * speed * Time.fixedDeltaTime);
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Waffe"))
		{
			//transform.eulerAngles = new Vector3(0,0,transform.eulerAngles.z +180);
			//Debug.Log(new Vector3(0,0,Mathf.Atan2((transform.position.y-coll.transform.position.y),(transform.position.x-coll.transform.position.x))*Mathf.Rad2Deg));
			transform.eulerAngles = new Vector3(0,0,Mathf.Atan2((transform.position.y-Player.transform.position.y),(transform.position.x-Player.transform.position.x))*Mathf.Rad2Deg-90);
			//trans = new Vector3(transform.position.x - Player.transform.position.x,transform.position.y-Player.transform.position.y,0).normalized;
			tag = "Seelenball";
		}

	}
}
