﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GurkeHP : MonoBehaviour {

	public float MaxHP;
    public float HP;
    public float HPPercent;
    public float HPWidth;
    public float MaxHPWidht;
    public float HPx;
    public float HPxS;

    public Image HPFill;
     

	// Use this for initialization
	void Start ()
    {

        HPFill = transform.Find("Fill").GetComponent<Image>();

        HPxS = HPFill.rectTransform.localPosition.x;
        MaxHPWidht = HPFill.rectTransform.sizeDelta.x;


    }
	
	// Update is called once per frame
	void Update ()
    {
        HP = transform.parent.parent.GetComponent<GurkeBoss>().HP;

        HPPercent = 100 * HP / MaxHP;
        HPWidth = HPPercent * MaxHPWidht / 100;

        HPx = (MaxHPWidht - HPWidth) / 2;

        HPFill.rectTransform.sizeDelta = new Vector2(HPWidth, HPFill.rectTransform.sizeDelta.y);
        HPFill.rectTransform.localPosition = new Vector3(HPxS - HPx, HPFill.rectTransform.localPosition.y, HPFill.rectTransform.localPosition.z);

	}
}
