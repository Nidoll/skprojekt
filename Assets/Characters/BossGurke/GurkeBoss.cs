﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GurkeBoss : MonoBehaviour {

	public GameObject GurkeFake;
	public Vector3 Rotation;
	public Animator ani;
	public float distance;
	public GameObject Player;
	public float dis;
	public float movespeed;
	public float chargespeed;
	public string Attack;
	private float timer;
	public Vector3 moveDir;
	public Vector3 moveDirCharge;

	public float distanceRL;
	public float distanceUD;
	public bool dontR;
	public bool dontU;
	public bool dontD;
	public bool dontL;
	public Vector3 PlayerP;
	public float HP;
	public GurkeHP SHP;
	public Vector3 Goal;
	private int hitcount;
	public bool notdone;
	private bool berühren;
	public GameObject Waren;
	public GameObject ObjectToDestroy;
	public GameObject[] ObjectsToSpawn = new GameObject[2];
	
	void Start()
	{
		Attack = "Stop";
		berühren = false;
		HP = 100;

		DontDestroyOnLoad(gameObject);
		gameObject.SetActive(false);
	}
	void FixedUpdate()
	{
		//if( Attack != "Stop")
		GetComponent<Rigidbody2D>().MovePosition(transform.position + moveDir*movespeed*Time.fixedDeltaTime);
	}
	void Update()
	{
		if(HP <= 0)
		{
			for(int i = 0; i < 2; i++)
			{
				Instantiate(ObjectsToSpawn[i], transform.position, Quaternion.identity);
			}
			Instantiate(Waren,transform.position,Quaternion.identity);
			Destroy(GameObject.Find("Steine boss"));
			//Destroy(ObjectToDestroy);
			Destroy(gameObject);
		}
		if((Player.transform.position-transform.position).normalized.x>0)
			GetComponent<SpriteRenderer>().flipX = true;
		else
			GetComponent<SpriteRenderer>().flipX = false;
		RayCast();
		distance = Mathf.Sqrt(Mathf.Pow(Player.transform.position.x - transform.position.x,2) + Mathf.Pow(Player.transform.position.y - transform.position.y,2));
		if(distance <= dis)
			ani.Play("BessererAttack");
		else
			ani.Play("GurkeLaufen");
		if(distance >= 10 && Attack != "Charge" && Attack != "Charge2")
		{
			Attack = "Charge";
			notdone = true;
			//PlayerP = Player.transform.position;
		}
		if(Attack == "Charge")
			Charge();
		if(Attack == "Normal")
			moveDir = (Player.transform.position-transform.position).normalized;
		if(Attack != "Charge" && Attack != "Stop")
		{
			movespeed = 1.5f;
			if(dontR == true)
			{
				if(moveDir.normalized.x>0)
				//moveDir.x =0;
            	moveDir.x=-1;
			}
			if(dontL == true)
			{
				if(moveDir.normalized.x<0)
				//moveDir.x =0;
            	moveDir.x=1;
			}
			if(dontU == true)
			{
				if(moveDir.normalized.y>0)
				//moveDir.y =0;
            	moveDir.y=-1;
			}
			if(dontD == true)
			{
				if(moveDir.normalized.y<0)
				//moveDir.y =0;
            	moveDir.y=1;
			}
		}
		if(Attack == "Stop")
		{
			moveDir = ((transform.position-Player.transform.position).normalized);
			movespeed = 0.5f;
		}
		transform.rotation = new Quaternion(0, 0, 0, 0);
		ChargeAttack2();
	}
	void Charge()
	{
		if(Attack != "Stop")
		{
		if(notdone)
		{
			Vector3 GPosition = transform.position;
			PlayerP = Player.transform.position;
			Goal = PlayerP + (PlayerP-GPosition).normalized*5;
			notdone = false;
		}
		if((Mathf.Sqrt(Mathf.Pow(Goal.x-transform.position.x,2)+Mathf.Pow(Goal.y-transform.position.y,2))) > 0.5)
		{
			moveDir = (Goal-transform.position).normalized;
			movespeed = chargespeed;
		}
		else
			Attack = "Normal";
	
		}
	}
	public void CollisionStuff(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Player.GetComponent<PlayerController>().getDamage(0.1f,transform.position);
			if(berühren == false)
			{
				Debug.Log("jawollo");
				berühren = true;
				Attack = "Stop";
			}
			else
			{
				Attack = "Normal";
				berühren = false;
			}
		}
		if(coll.CompareTag("Waffe"))
		{
			HP -= 10f;
			hitcount += 1;
		}
	}

	void RayCast()
	{
		Debug.DrawRay(transform.position,Vector3.right*distanceRL);
		Debug.DrawRay(transform.position,Vector3.right*distanceRL*-1);
		Debug.DrawRay(transform.position,Vector3.up*distanceUD);
		Debug.DrawRay(transform.position,Vector3.up*distanceUD*-1);

		Vector2 origin = new Vector2(transform.position.x,transform.position.y);
		RaycastHit2D SideR = Physics2D.Raycast(origin,Vector2.right,distanceRL,LayerMask.GetMask("Wall"));
        RaycastHit2D SideL = Physics2D.Raycast(origin,Vector2.right*-1,distanceRL,LayerMask.GetMask("Wall"));
        RaycastHit2D SideU = Physics2D.Raycast(origin,Vector2.up,distanceUD,LayerMask.GetMask("Wall"));
        RaycastHit2D SideD = Physics2D.Raycast(origin,Vector2.up*-1,distanceUD,LayerMask.GetMask("Wall"));
		if(Attack != "Charge")
		{
			if(SideR)
		{
			dontR = true;
		}else
		dontR = false;
        if(SideL)
        {
			dontL = true;
        }else
		dontL = false;
        if(SideU)
        {
			dontU = true;
        }else
		dontU = false;
        if(SideD)
        {
			dontD = true;
        }else
		dontD = false;
		}
		
		
	}
	void ChargeAttack2()
	{
		int random = 0;
		if(hitcount == 3 && Attack != "Charge")
		{
			Attack = "Charge2";
			moveDir = (transform.position-Player.transform.position).normalized;
			movespeed = chargespeed;
			if(Vector3.Distance(Player.transform.position,transform.position)>20 && random == 0)
			{
				random = Random.Range(1,4);
				switch(random)
				{
					case 1:
					transform.position = new Vector3(Player.transform.position.x+20,Player.transform.position.y,0);
					break;
					case 2:
					transform.position = new Vector3(Player.transform.position.x-20,Player.transform.position.y,0);
					break;
					case 3:
					transform.position = new Vector3(Player.transform.position.x,Player.transform.position.y+20,0);
					break;
					case 4:
					transform.position = new Vector3(Player.transform.position.x,Player.transform.position.y-20,0);
					break;
				}
				Attack = "Normal";
				random = 0;
				hitcount = 0;
			}
		}

	}

	
}
