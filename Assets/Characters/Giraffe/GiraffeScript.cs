﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiraffeScript : MonoBehaviour {

	public bool ContactToPlayer;
	public GameObject Canvas;
	public GameObject CheckPoint;


	// Use this for initialization
	void Start () 
	{
		Canvas = transform.Find("Canvas").gameObject;
		CheckPoint = transform.Find("CheckPoint").gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(ContactToPlayer && Input.GetKeyDown(KeyCode.E))
		{
			PlayerManager.instance.CheckpointPosition = CheckPoint.transform.position;
		}
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Canvas.SetActive(!Canvas.activeInHierarchy);
			ContactToPlayer = true;
		}
	}

	public void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Canvas.SetActive(!Canvas.activeInHierarchy);
			ContactToPlayer = false;
		}
	}
}
