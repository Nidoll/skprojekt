﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaendlerScript : MonoBehaviour {

	public int HaendlerPhase = 0;
	public const int NumberOfDialogs = 2;
	public GameObject[] Dialogs = new GameObject[NumberOfDialogs];
	public Vector2 NewHaendlerPosition;
	public Inventory Inventory;

	// Use this for initialization
	void Start () 
	{

		DontDestroyOnLoad(gameObject);

		for(int i = 0; i < NumberOfDialogs; i++)
		{
			Dialogs[i] = transform.Find("DialogTrigger" + i).gameObject;
		}

		for(int i = 1; i < NumberOfDialogs; i++)
		{
			Dialogs[i].SetActive(false);
		}

		Inventory = PlayerManager.instance.Canvas.GetComponent<Inventory>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector2(-45f,20f);
		switch(HaendlerPhase)
		{
			case 0:
				//Standart Phase
				break;
			
		}
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			switch(HaendlerPhase)
			{
				case 0:
					HaendlerPhase = 1;
					//Dialogs[0].SetActive(false);
					Inventory.AddItem(PlayerManager.instance.HealPot);
					break;
				case 1:
					Debug.Log("Test1");
					if(Inventory.TestForItem(PlayerManager.instance.HändlerWaren, 1))
					{
						Debug.Log("Test");
						HaendlerPhase = 2;
						Dialogs[1].SetActive(true);
						PlayerManager.instance.Player.GetComponent<PlayerController>().HpCap = 0.4f;
						PlayerManager.instance.Player.GetComponent<PlayerController>().currentHP = 0.4f;
					}
					break;
			}
		}
		
	}

	
}
