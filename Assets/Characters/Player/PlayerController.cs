﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    //---Inventory---
    public Inventory other;
    private Item Lolli_I;
    private Item Coin_I;
    private Item HealPot_I;
    private Item Gurke_I;

    //---GameManagemant---
    private bool dialogphase;
    public GameObject MainCamera;
    private float time2;
    public Material PlayerM;
	public Material NormalM;
    private bool inside;
    private ParticleSystem PS;

    //---Movemant---
    public Vector3 PlayerMoveDirection;
    public Rigidbody2D PlayerRigidbody;
    public int PlayerMovespeed;
    private bool moveable = true;

    //---Playeranimator---
    public Animator PlayerAnimator;
    
    //---Health System---
    public float HpCap;
    public float MaxHp;
    public float currentHP;
    public GameObject Healthbar;
    private string DamagePhase;
    
    //---Knockback---
    public float knockspeed;
    public float test;
    public GameObject Fake;
    private Color color;
    private bool Imunitie;
    public float blinkamount;
    public float blinktime;
    public float transparenze;
    private Vector3 knockdir;
    private bool hit;
    
    //---Kampf System---
    public Vector3 MousePosition;
    public float zWeapon;
    public string WeaponDirection;
    public int WeaponDamage = 1;

    //--- Sound System---

    public AudioClip WalkSound;
    private AudioSource AudioSourcePlayer;
    private bool SoundIsPlaying = false;
    private bool SingleSoundIsPlaying = false;
    public AudioClip AttackSound;
    public bool fix = false;

    
    void Start ()
    {  
        inside = true;
        GetComponent<SpriteRenderer>().material = PlayerM;
        color = GetComponent<SpriteRenderer>().color;
        test = 0;
        currentHP = HpCap;
        DamagePhase = "normal";
        PS = transform.GetComponentInChildren<ParticleSystem>();
        Lolli_I = PlayerManager.instance.Lolli;
        Coin_I = PlayerManager.instance.Coin;
        HealPot_I = PlayerManager.instance.HealPot;
        Gurke_I = PlayerManager.instance.Gurke; 

        AudioSourcePlayer = GetComponent<AudioSource>();

        DontDestroyOnLoad(gameObject);
    }	
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            GetComponent<BoxCollider2D>().enabled = !GetComponent<BoxCollider2D>().enabled;
        }
        

        if(DamagePhase == "Normal")
        {
            gameObject.transform.eulerAngles = Vector3.zero;
            Fake.gameObject.transform.eulerAngles = Vector3.zero;
        }
        if(currentHP <= 0)
        {
            if(fix == false)
            {
                Destroy(GameObject.Find("Antagonist 1").gameObject);
                fix = true;
            }
            /* PlayerManager.instance.FateAni.SetTrigger("Fate");
            PlayerManager.instance.FateAni.SetBool("Teleport", false);
            PlayerManager.instance.FateAni.SetTrigger("FateBack");*/
            transform.position = PlayerManager.instance.CheckpointPosition;
            StartCoroutine(Delay());
        }
        if(PlayerMoveDirection != Vector3.zero)
        {
            PS.Play();
        }else{
            PS.Pause();
            PS.Clear();
        }
        HealthDisplay();
        AnimationAndShit();
        Sound();
    }
    void FixedUpdate ()
    {   
        Movement();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
 
        switch(collision.tag)
        {
            case"Lolli":
                Destroy(collision.gameObject);
                other.AddItem(Lolli_I);
                break;
            case"Coin":
                Destroy(collision.gameObject);
                other.AddItem(Coin_I);
                break;
            case"HealPot":
                Destroy(collision.gameObject);
                other.AddItem(HealPot_I);
                break;
            case"Enemy":
                hit = true;
                knockdir = (transform.position-collision.transform.position).normalized;
                break;
            case"SkeletonBall":
                hit = true;
                knockdir = (transform.position-collision.transform.position).normalized;
                getDamage(0.05f,collision.transform.position);
                break;
            case"Gurke":
                Destroy(collision.gameObject);
                other.AddItem(Gurke_I);
                break;
            case"Sack":
                Destroy(collision.gameObject);
                other.AddItem(PlayerManager.instance.HändlerWaren);
                break;
            case"Fragment1":
                Destroy(collision.gameObject);
                other.AddItem(PlayerManager.instance.Fragment1);
                break;
            case"Fragment2":
                Destroy(collision.gameObject);
                other.AddItem(PlayerManager.instance.Fragment2);
                break;
            case"Fragment3":
                Destroy(collision.gameObject);
                other.AddItem(PlayerManager.instance.Fragment3);
                break;
            case"Key":
                Destroy(collision.gameObject);
                other.AddItem(PlayerManager.instance.Key_I);
                break;
        
        }
     
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.transform.CompareTag("Gegner") || collision.transform.CompareTag("Seelenball"))
        {
            hit = false;
        }

    }
    public void SwitchDialogPhase()
    {
      
        dialogphase=!dialogphase;
    }
    public void Movement()
    {
      
        if (dialogphase == false)
        {
            //if(DamagePhase == "normal")
           // {               
                PlayerRigidbody.MovePosition(transform.position + PlayerMoveDirection.normalized * PlayerMovespeed * Time.fixedDeltaTime);              
           // }
            /* else
            {
                time2 += Time.deltaTime;
                if(time2 <= 0.1)
                {
                    PlayerRigidbody.MovePosition(transform.position + knockdir * knockspeed * Time.fixedDeltaTime);
                }
                else
                {
                    DamagePhase = "normal";
                    time2 = 0;
                }
            }*/
        }
    }
    public void AnimationAndShit()
    {
        //TODO ueberarbeiten
        if(PlayerAnimator.GetBool("Attack") == true)
        {
            moveable = false;
        }else{
            moveable = true;
        }

        PlayerMoveDirection = Vector3.zero;

        if(moveable == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                PlayerMoveDirection = PlayerMoveDirection + new Vector3(0, 1, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                PlayerMoveDirection = PlayerMoveDirection + new Vector3(0, -1, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {
                PlayerMoveDirection = PlayerMoveDirection + new Vector3(-1, 0, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                PlayerMoveDirection = PlayerMoveDirection + new Vector3(1, 0, 0);
            }
        }
        

        MainCamera.transform.position = new Vector3(transform.position.x,transform.position.y, -10);
        transform.rotation = new Quaternion(0, 0, 0, 0);

        if (dialogphase == false)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                
                PlayerAnimator.SetBool("Attack",true);
                MousePosition =  PlayerManager.instance.cam.ScreenToWorldPoint(Input.mousePosition);
                    zWeapon = Mathf.Atan2((MousePosition.y-transform.position.y),(MousePosition.x-transform.position.x))*Mathf.Rad2Deg-90;
                  
                    if(zWeapon < 0)
                    {
                        zWeapon = 360 + zWeapon;
                    }
                    if(zWeapon > 45 && zWeapon < 135)
                    WeaponDirection = "Left";
                    else if(zWeapon > 135 && zWeapon < 225)
                    WeaponDirection = "Front";
                    else if(zWeapon > 225 && zWeapon < 315)
                    WeaponDirection = "Right";
                    else
                    WeaponDirection = "Up";

                    switch(WeaponDirection)
                    {
                        //TODO Variablen ueberarbeiten (Strings anstatt bools)
                        case "Left":
                            PlayerAnimator.SetBool("AttackL",true);
                            break;
                        case "Front":
                            PlayerAnimator.SetBool("AttackF",true);
                            break;
                        case "Right":
                            PlayerAnimator.SetBool("AttackR",true);
                            break;                          
                        case "Up":
                            PlayerAnimator.SetBool("AttackU",true);
                            break;
                    }
                
            }
    
            if(PlayerMoveDirection.x > 0)
            {
                PlayerAnimator.Play("WalkR");
                PlayerAnimator.SetInteger("StandDirection",1);
            }else if(PlayerMoveDirection.x < 0)
            {
                PlayerAnimator.Play("WalkL");
                PlayerAnimator.SetInteger("StandDirection",1);
            }else if(PlayerMoveDirection.y > 0 && PlayerMoveDirection.x ==0)
            {
                PlayerAnimator.Play("WalkB");
                PlayerAnimator.SetInteger("StandDirection",4);
            }else if(PlayerMoveDirection.y < 0 && PlayerMoveDirection.x ==0)
            {
                PlayerAnimator.Play("WalkF");
                PlayerAnimator.SetInteger("StandDirection",2);
            }else if(PlayerMoveDirection == Vector3.zero)
            {
                PlayerAnimator.SetInteger("StandDirection",0);
            }

        }     
             
    }
    void HealthDisplay()
    {
        Healthbar.GetComponentInChildren<Image>().fillAmount = MaxHp * currentHP;
    }
    public void getDamage(float Damage,Vector3 EnemyPosition)
    {
        if(Imunitie == false)
        {
            if(Damage == 0)
            {
                currentHP -= 0.05f;
            }else{
                currentHP -= Damage;
            }
            if(currentHP > 0)
            {
                knockdir = transform.position-EnemyPosition;
                DamagePhase = "k";
                test += 1f;
                StartCoroutine(DoBlinks(blinkamount,blinktime));
            }
        }
    }
    IEnumerator DoBlinks(float amount, float blinkTime)
    {
        Imunitie = true;
        bool transparent = false;
        while (amount > 0f)
        {
            amount -= 0.5f;
            if(transparent == false)
            {
                color.a = transparenze;
            }else{
                color.a = 1f;
            }
            GetComponent<SpriteRenderer>().color = color;
            GetComponent<SpriteRenderer>().material.color = color;
            transparent = !transparent;
            yield return new WaitForSeconds(blinkTime);
        }
        color.a = 1f;
        GetComponent<SpriteRenderer>().color = color;
        GetComponent<SpriteRenderer>().material.color = color;
        Imunitie = false;
        yield break;
    } 
    public void MaterialChange()
    {

        if(inside == true)
        {
            GetComponent<SpriteRenderer>().material = NormalM;
        }else
        {
            GetComponent<SpriteRenderer>().material = PlayerM;
        }
        inside = !inside;
    }

    public void HealPlayer(float HealAmount)
    {
        if(HealAmount + currentHP > HpCap)
        {
            currentHP = HpCap;
        }else{
            currentHP += HealAmount;
        }
    } 

    public void Sound()
    {

        if(PlayerMoveDirection != Vector3.zero && SoundIsPlaying == false)
        {
            AudioSourcePlayer.clip =  WalkSound;
            AudioSourcePlayer.Play();
            SoundIsPlaying = true;
        }

        if(PlayerMoveDirection == Vector3.zero && SoundIsPlaying == true)
        {
            AudioSourcePlayer.Stop();
            SoundIsPlaying = false;  
        }

        if(PlayerAnimator.GetBool("Attack") && SingleSoundIsPlaying == false)
        {
            AudioSourcePlayer.PlayOneShot(AttackSound);
            SingleSoundIsPlaying = true;
            StartCoroutine(WaitTime());
            
        }

    }

    private IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1f);
        SingleSoundIsPlaying = false;
        yield break;
    }
    
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        currentHP = HpCap;
        yield break;
    }
}
