﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour {

    public float MaxHP;
    public float HP;
    public float HPPercent;
    public float HPWidth;
    public float MaxHPWidht;
    public float HPx;
    public float HPxS;

    public Image HPFill;
     

	// Use this for initialization
	void Start ()
    {

        HPFill = transform.Find("Fill").GetComponent<Image>();
        HPxS = HPFill.rectTransform.position.x;
        MaxHPWidht = HPFill.rectTransform.sizeDelta.x;

        DontDestroyOnLoad(gameObject);

    }
	
	// Update is called once per frame
	void Update ()
    {
        
        if (MaxHP != HP)
        {

            //MaxHP = transform.parent.parent.GetComponent<Antagonist_Tutorial>().MaxHP;

        }

        HP = transform.parent.parent.GetComponent<Antagonist_Tutorial>().HP;

        HPPercent = 100 * HP / MaxHP;
        HPWidth = HPPercent * MaxHPWidht / 100;

        HPx = (MaxHPWidht - HPWidth) / 2;



        HPFill.rectTransform.sizeDelta = new Vector2(HPWidth, HPFill.rectTransform.sizeDelta.y);
        HPFill.rectTransform.position = new Vector3(HPxS - HPx, HPFill.rectTransform.position.y, HPFill.rectTransform.position.z);

	}
}
