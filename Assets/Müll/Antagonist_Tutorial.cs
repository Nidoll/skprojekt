﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antagonist_Tutorial : MonoBehaviour {

    public float HP;
    public float MaxHP;
    public GameObject Player;
    public GameObject KreisenAttack;
    private string Attack;
    public float KreisenSpeed;
    public GameObject SchussAttack;
    public float SchussSpeed;
    public GameObject Projectile;
    public bool shild;
	void Start ()
    {
        shild = true;
        HP = 1000;
        MaxHP = 1000;

	}
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        Attack = "Kreisen";
        if ( Input.GetKeyDown(KeyCode.Alpha2))
        Attack = "Schuss";

        if(HP <= 0)
        Destroy(gameObject);
	}
    public void DamageTaken(float Damage)
    {
        HP -= Damage;
    }
    void FixedUpdate()
    {
        switch (Attack)
        { 
            case "Kreisen":
            KreisenA();
            break;
            case "Schuss":
            SchussA();
            KreisenAttack.SetActive(false);  
            break;
        }
    }
    public void KreisenA()
    {
        KreisenAttack.SetActive(true);
        KreisenAttack.transform.Rotate(new Vector3(0,0,1)*Time.deltaTime*KreisenSpeed);
    }
    public void SchussA()
    {
        float z = Mathf.Atan2(Player.transform.position.y - SchussAttack.transform.position.y, Player.transform.position.x - SchussAttack.transform.position.x) * Mathf.Rad2Deg -90;
        SchussAttack.transform.eulerAngles = new Vector3(0,0,z);
        Instantiate(Projectile,SchussAttack.transform.position,SchussAttack.transform.rotation);
    }
}
