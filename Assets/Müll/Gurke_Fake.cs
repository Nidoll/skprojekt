﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gurke_Fake : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Waffe"))
		transform.parent.GetComponent<GurkeBoss>().CollisionStuff(coll);
		if(coll.CompareTag("Player"))
		transform.parent.GetComponent<GurkeBoss>().CollisionStuff(coll);
		

	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		transform.parent.GetComponent<GurkeBoss>().CollisionStuff(coll);
	}
}
