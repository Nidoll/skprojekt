﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickArea : MonoBehaviour {

	public int ItemSlot;
	public Button UseButton;
	public Button InfoButton;
	public Inventory Inv;
	public GameObject Test;

	// Use this for initialization
	void Start () 
	{
		Test = transform.parent.parent.gameObject;
		Inv = Test.GetComponent<Inventory>();

		UseButton = transform.Find("UseButton").GetComponent<Button>();
		InfoButton = transform.Find("InfoButton").GetComponent<Button>();

		UseButton.onClick.AddListener(Use);
		InfoButton.onClick.AddListener(Info);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerExit()
	{
		gameObject.SetActive(false);
	}

	public void OnClick()
	{
		
	}

	public void Open(int Slot)
	{
		ItemSlot = Slot;
	}

	public void Use()
	{
		Inv.UseF(ItemSlot);
	}

	public void Info()
	{
		Inv.InfoF(ItemSlot);
	}
}
