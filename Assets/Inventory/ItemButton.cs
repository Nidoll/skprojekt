﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemButton : MonoBehaviour {

    public GameObject ItemG;
    private Button ItemB;

    public GameObject UsingG;
    private Button UsingB;

    public GameObject InfoG;
    private Button InfoB;

    public GameObject EquipG;
    private Button EquipB;

    public GameObject CloseG;
    private Button CloseB;

    public Inventory inventoryO;

    public GameObject Parent;

    public int SlotNumber;
    public string NumberS;
    public string[] StringArray = new string[3];
    public string NumberSingle;

    public GameObject Inventory;
    public GameObject InfoArea;

    public GameObject Test;

    public Sprite BackgroundA;
    public Sprite BackgroundN;
    public GameObject ClickOverlay;
    public string ItemSlot;
    public int ItemSlot_int;

    
	// Use this for initialization
	void Start()
    {
        ClickOverlay = transform.parent.parent.transform.Find("ClickOverlay").gameObject;
        ItemSlot = transform.parent.name.Substring(transform.parent.name.Length - 2);
        ItemSlot_int = int.Parse(ItemSlot);
    }

    public void OnPointerClick()
    {
        ClickOverlay.SetActive(true);
        ClickOverlay.transform.position = Input.mousePosition;
        ClickOverlay.GetComponent<ClickArea>().Open(ItemSlot_int);
    }

    public void OnPointerEnter()
    {
        transform.parent.Find("BackroundImage").GetComponent<Image>().sprite = BackgroundA;
    }

    public void OnPointerExit()
    {
        transform.parent.Find("BackroundImage").GetComponent<Image>().sprite = BackgroundN;
    }
	
	// Update is called once per frame
	void Update ()
    {

        

    }

    void CloseOnClick()
    {

        UsingG.SetActive(false);

    }

    void EquipOnClick()
    {

    }

    public void InfoOnClick()
    {
       
        //inventoryO.InfoF(SlotNumber);

    }

    void UseOnClick()
    {

        
        
    }
}
