﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseButtonScript : MonoBehaviour {

    private Button CloseButton;
    private GameObject Window;

	// Use this for initialization
	void Start ()
    {

        CloseButton = transform.GetComponent<Button>();
        CloseButton.onClick.AddListener(CloseWindow);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void CloseWindow()
    {

        transform.parent.transform.gameObject.SetActive(false);

    }

    //test
}
