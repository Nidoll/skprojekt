﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    public const int numItemSlots = 18;

    public GameObject Inventar;
    public bool UITest = true;

    public Image[] ItemImages = new Image[numItemSlots];
    public Item[] Items = new Item[numItemSlots];
    public Sprite[] Sprites = new Sprite[numItemSlots];
    public int[] Count = new int[numItemSlots];
    public GameObject[] ItemSlots = new GameObject[numItemSlots];
    public Text[] Amounts = new Text[numItemSlots];

    private GameObject ItemSlot;
    private string ItemSlotNummer;
    private GameObject ItemImage;

    private int ItemAnzahl;
    private GameObject itemNummer;

    private GameObject UseButton;

    public Image InfoAreaImage;
    public Text InfoAreaText;

    public GameObject InfoArea;
    public GameObject ClickOverlay;

    public Sprite Transparent;


    public void Start()
    {
        DontDestroyOnLoad(gameObject);

        for(int i = 0; i < Items.Length; i++)
        {
            ItemSlotNummer = i.ToString();

            if(i < 10)
            {
                ItemSlots[i] = GameObject.Find("ItemSlot0" + ItemSlotNummer);
            }else{
                ItemSlots[i] = GameObject.Find("ItemSlot" + ItemSlotNummer);
            }

            ItemImages[i] = ItemSlots[i].transform.Find("ItemImage").gameObject.GetComponent<Image>();
            Amounts[i] = ItemSlots[i].transform.Find("ItemNumber").gameObject.GetComponent<Text>();
        }

        //InfoArea = GameObject.Find("InfoArea");
        InfoAreaImage = InfoArea.transform.Find("ItemImage").gameObject.GetComponent<Image>();
        InfoAreaText = InfoArea.transform.Find("Text").gameObject.GetComponent<Text>(); 

        ClickOverlay = GameObject.Find("ClickOverlay");

        ClickOverlay.SetActive(false);
        InfoArea.SetActive(false);
        Inventar.SetActive(false);

    }

    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.I))
        {

           if (Inventar.activeInHierarchy == false)
            {

                Inventar.SetActive(true);

            }
            else
            {
                ClickOverlay.SetActive(false);
                InfoArea.SetActive(false);
                Inventar.SetActive(false);
            }

            

        }

        UpdateAll();
        

    }

    public void UpdateAll()
    {

        if(Inventar.activeInHierarchy)
        {
            for(int i = 0; i < Items.Length; i++)
            {
                if(Items[i] != null)
                {
                    ItemImages[i].sprite = Items[i].sprite;
                    Amounts[i].text = Count[i].ToString();
                }else{
                   // Amounts[i].text = "00";
                   // ItemImages[i].sprite = Transparent;
                }
                
            }
        }

    }

    public void AddItem(Item itemToAdd)
    {
        for (int i = 0; i < Items.Length; i++)
        {

            if (Items[i] == itemToAdd)
            {

                Debug.Log("Why");
                Count[i] += 1;
                return;

            }
            

        }

        for(int i = 0; i < Items.Length; i++)
        {
            if (Items[i] == null)
            {

                Debug.Log("Because");
                Items[i] = itemToAdd;
                Sprites[i] = itemToAdd.sprite;
                Count[i] = 1;
                return;

            }
        }
    }

    public void RemoveItem(Item itemToRemove)
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if(Items[i] == itemToRemove)
            {
                if(Count[i] == 1)
                {
                    Items[i] = null;
                    Count[i] = 0;
                }else{
                    Count[i] -= 1;
                }

            }
        }
    }

   
    public void UseF(int SlotNummer)
    {

        if(Items[SlotNummer].Effect != "No")
        {
            Items[SlotNummer].EffectF();
            RemoveItem(Items[SlotNummer]);
        }

    }

    public void InfoF(int SlotNummer)
    {

        if (InfoArea.activeInHierarchy == false)
        {

            InfoArea.SetActive(true);
            InfoAreaImage.sprite = Items[SlotNummer].sprite;
            InfoAreaText.text = Items[SlotNummer].ItemInfoText; 

        }


    }

    public bool TestForItem(Item ItemToTest, int AmountOfItems)
    {
        
        for (int i = 0; i < Items.Length; i++)
        {
            if(Items[i] == ItemToTest)
            {
                if(Count[i] >= AmountOfItems)
                {
                    return true;
                }else{
                    return false;
                }

            }
            
        }
        return false;
    }
}
