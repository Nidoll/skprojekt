﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class PlayerManager : MonoBehaviour {

	#region Singleton

	public static PlayerManager instance;

	public Scene GameScene;
	public GameObject Player;
	public Item Lolli;
	public Item Coin;
	public GameObject Seelenball;
	public GameObject Canvas;
	public GameObject TutorialBossBar;
	public float FPS;
	public Text FPST;
	public Camera cam;
	public Item HealPot;
	public Animator FateAni;
	public Tilemap BorderTilemap;
	public Item Gurke;
	public GameObject Gurke_Item;
	public Canvas Scene_Canvas;
	public AudioClip DoorSound;
	public Vector2 CheckpointPosition;
	public Item HändlerWaren;
	public Item Fragment1;
	public Item Fragment2;
	public Item Fragment3;
	public Vector2 SpawnPoint;
	public Item Key_I;
	public GameObject GurkeBoss;
	public bool MenuIsOpen;
	

	void Awake()
	{
		instance = this;
	}

	#endregion

	
	

	void Start()
	{
		DontDestroyOnLoad(gameObject);
	}

	void Update()
	{
		//FPS = 1/Time.deltaTime;
		//FPST.text = FPS.ToString();

		

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Canvas.transform.Find("MainMenu").gameObject.SetActive(!Canvas.transform.Find("MainMenu").gameObject.activeInHierarchy);
			if(Canvas.transform.Find("MainMenu").gameObject.activeInHierarchy)
			{
				GetComponent<AudioSource>().Stop();
			}else{
				GetComponent<AudioSource>().Play();
			}
		}

		if(Input.GetKeyDown(KeyCode.L))
		{
			PlayerManager.instance.FateAni.SetTrigger("FateBack");
		}

	}

	public void LoadGameScene()
	{
		SceneManager.LoadScene("RichtigeScene", LoadSceneMode.Single);
		Player.transform.position = SpawnPoint;
	}

	public void PlayCredits()
	{
		Player.transform.position = new Vector2(5000f,5000f);
		Player.SetActive(false);
		GameObject.Find("Main Camera").transform.position = new Vector2(5000f,5000f);
		Canvas.SetActive(false);
		GameObject.Find("Main Camera").GetComponent<VideoPlayer>().Play();
	}


}
