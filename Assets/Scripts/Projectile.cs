﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float speed;
	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().MovePosition(transform.position + transform.up*speed*Time.deltaTime);
		Destroy(gameObject,5f);
	}
}
