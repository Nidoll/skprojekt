﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundButton : MonoBehaviour {

	public GameObject Objeckt;
	public Sprite sprite;
	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Objeckt.GetComponent<Eule>().Action();
			GetComponent<SpriteRenderer>().sprite = sprite;
		}
	}
}
