﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject {

    public string Effect;
    public Sprite sprite;

    [TextArea(3,10)]
    public string ItemInfoText;

    public void EffectF()
    {
        switch(Effect)
        {    
            case"Heal":
                Heal();
                break;
        }
    }
    public void Heal()
    {
        PlayerManager.instance.Player.GetComponent<PlayerController>().HealPlayer(0.1f);
    }
}
