﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerToSpawnObject_Script : MonoBehaviour {

	public GameObject ObjectToChange;

	void Start () 
	{
		
	}
	
	
	void Update () 
	{
		
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			ObjectToChange.SetActive(!ObjectToChange.activeInHierarchy);
			Destroy(gameObject);
		}
	}
}
