﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InOutTrigger : MonoBehaviour {

	public Material PlayerM;
	public Material NormalM;
	public GameObject Player;
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{	
			Player.GetComponent<PlayerController>().MaterialChange();
		}
	}	
}
