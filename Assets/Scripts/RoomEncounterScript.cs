﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomEncounterScript : MonoBehaviour {

	public GameObject[] EnemysToSpawn = new GameObject[6];
	public GameObject[] EnemysSpawnpoints = new GameObject[6];
	public SpriteRenderer[] SpriteRenderers = new SpriteRenderer[6];
	public GameObject[] BlockingObjects = new GameObject[3];
	public bool RoomCleared = false;
	public bool RoomEngaged = false;
	public bool EnemysAlive = true;
	public GameObject[] Enemys = new GameObject[6];
	public GameObject[] ObjectsToDrop = new GameObject[2];
	public bool GurkeFight;

	void Start () 
	{
		for(int i = 1; i <= 6; i++)
		{
			EnemysSpawnpoints[i - 1] = transform.Find("EnemySpawnpoint" + i).gameObject;
			SpriteRenderers[i - 1] = EnemysSpawnpoints[i - 1].GetComponent<SpriteRenderer>(); 
			SpriteRenderers[i - 1].enabled = false;
		}

		for(int i = 1; i <= 3; i++)
		{
			BlockingObjects[i - 1] = transform.Find("BlockingObject" + i).gameObject;
			BlockingObjects[i - 1].SetActive(false);
		}
	}
	
	
	void Update () 
	{
		if(EnemysSpawnpoints[0].transform.childCount == 0 && EnemysSpawnpoints[1].transform.childCount == 0 && EnemysSpawnpoints[2].transform.childCount == 0  && EnemysSpawnpoints[3].transform.childCount == 0 && EnemysSpawnpoints[4].transform.childCount == 0 && EnemysSpawnpoints[5].transform.childCount == 0 && RoomEngaged == true)
		{
			for(int i = 0; i < 2; i++)
			{
				
			}
			Destroy(gameObject);
		}

		if(RoomEngaged && PlayerManager.instance.Player.GetComponent<PlayerController>().currentHP <= 0)
		{
			Debug.Log("Trigger");
			for(int i = 1; i <= 3; i++)
			{
				BlockingObjects[i -1].SetActive(false);
			}

			for(int i = 0; i <= 5; i++)
			{
				Destroy(Enemys[i]);
			}

			RoomEngaged = false;
			GetComponent<BoxCollider2D>().enabled = true;
		}
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			if(GurkeFight)
			{
				PlayerManager.instance.GurkeBoss.transform.position = EnemysSpawnpoints[0].transform.position;
				PlayerManager.instance.GurkeBoss.SetActive(true);
			}

			for(int i = 0; i <= 5; i++)
			{
				Enemys[i] = Instantiate(EnemysToSpawn[i], EnemysSpawnpoints[i].transform.position, Quaternion.identity, EnemysSpawnpoints[i].transform);
			}

			for(int i = 0; i <= 2; i++)
			{
				BlockingObjects[i].SetActive(true);
			}

			GetComponent<BoxCollider2D>().enabled = false;
			RoomEngaged = true;
		}
	}
}
