﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchildScript : MonoBehaviour {

	public GameObject Canvas;
	void Start () 
	{
		Canvas = transform.Find("Canvas").gameObject;
	}
	
	
	void Update () 
	{
		
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Canvas.SetActive(true);
		}
	}

	public void OnTriggerExit2D(Collider2D coll)
	{
		if(coll.CompareTag("Player"))
		{
			Canvas.SetActive(false);
		}
	}
}
