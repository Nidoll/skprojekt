﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gurke_Tester : MonoBehaviour {

	public int Amount;
	public Item ItemToTest;
	public Gate GateObject;
	private bool active = false;
	private bool Used = false;

	void Start()
	{

	}

	void Update()
	{
			if(Input.GetKeyDown(KeyCode.E) && active == true && Used == false)
			{
				if(PlayerManager.instance.Canvas.GetComponent<Inventory>().TestForItem(ItemToTest, Amount))
				{
					Debug.Log("Open");
					GateObject.OpenGate();
					for(int i = 0; i < 5; i++)
					{
						PlayerManager.instance.Canvas.GetComponent<Inventory>().RemoveItem(ItemToTest);
					}
				}else{
					Debug.Log("Dont Open");
				}
			}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player") && Used == false)
		{
			transform.Find("Canvas").gameObject.SetActive(true);
			active = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.CompareTag("Player") && Used == false)
		{
			transform.Find("Canvas").gameObject.SetActive(false);
			active = false;
		}
	}
}
