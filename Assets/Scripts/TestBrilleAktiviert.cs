﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TestBrilleAktiviert : MonoBehaviour {

	private GameObject Player;
	private Tilemap Border;

	public bool TileInPosition;

	public Vector3Int IntMousePosition;
	private GameObject Camera;
	public Vector3 MousePosition;
	public int DistanceToPlayer;
	

	// Use this for initialization
	void Start () 
	{
		Player = PlayerManager.instance.Player;	
		Border = transform.Find("Border").GetComponent<Tilemap>();
		Camera = PlayerManager.instance.cam.gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		MousePosition = PlayerManager.instance.cam.ScreenToWorldPoint(Input.mousePosition);

		//IntMousePosition = new Vector3Int(Mathf.RoundToInt(MousePosition.x),Mathf.RoundToInt(MousePosition.y),0);
		IntMousePosition = new Vector3Int(Mathf.RoundToInt(Mathf.Floor(MousePosition.x)),Mathf.RoundToInt(Mathf.Floor(MousePosition.y)),0);
		

		if(Input.GetKeyDown(KeyCode.L))
		{
			Debug.Log(Border.GetTile(IntMousePosition));
			if(Border.GetTile(IntMousePosition) != null)
			{
				Debug.Log("Wall");
			}else{
				Debug.Log("No Wall");
			}
		}
	}
}
