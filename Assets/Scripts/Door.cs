﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public GameObject otherDoor;
	private GameObject Player;
	public bool active;
	public bool gooutside;
	public GameObject Spawnpoint;
	public bool TeleportB;
	private AudioSource DoorSoundSource;
	public AudioClip DoorSound;

	void Start()
	{
		
		Player = PlayerManager.instance.Player;
		active = true;

		Spawnpoint = otherDoor.transform.Find("Spawnpoint").gameObject;

		DoorSoundSource = GetComponent<AudioSource>();

	}

	void OnCollisionEnter2D(Collision2D coll)
	{
			
		if(coll.gameObject.name == "Player" && active == true)
		{
			DoorSoundSource.PlayOneShot(DoorSound);
			DoorSoundSource.PlayOneShot(PlayerManager.instance.DoorSound);
			PlayerManager.instance.FateAni.SetTrigger("Fate");
			TeleportB = true;
			//coll.transform.position = otherDoor.transform.Find("Spawnpoint").transform.position;
		}
	}

	void Update()
	{
		if(PlayerManager.instance.FateAni.GetBool("Teleport") && TeleportB)
		{
			PlayerManager.instance.FateAni.SetBool("Teleport", false);
			TeleportB = false;
			Player.transform.position = Spawnpoint.transform.position;
			PlayerManager.instance.FateAni.SetTrigger("FateBack");
			otherDoor.GetComponentInChildren<DoorRange>().DoorPassed();
			//if(gooutside == true)
			//Player.GetComponent<PlayerController>().MaterialChange();
		}
	}

	public void DissableDoor()
	{
		active = false;
	}
	public void EnableDoor()
	{
		active = true;
	}

}
