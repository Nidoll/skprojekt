﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour {

    public Dialog dialog;
    public GameObject Charakter;

    public void TriggerDialog()
    {
        
        FindObjectOfType<DialogSystem>().StartDialog(dialog,gameObject,true);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {

            TriggerDialog();

        }
    }

    
}
