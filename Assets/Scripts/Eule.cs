﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eule : MonoBehaviour {

	public bool a;
	private float time;
	public float distance;
	public float speed;

	void Start()
	{
		a = false;
		time = 0;
	}
	void FixedUpdate()
	{
		if(a == true)
		GetComponent<Rigidbody2D>().MovePosition(new Vector2(transform.position.x,transform.position.y) + Vector2.down * speed * Time.fixedDeltaTime);
	}
	void Update()
	{
		if(a == true)
		{
			time += Time.deltaTime;
			if(time >= distance)
			a = false;
		}
	}
	public void Action()
	{
		a = true;
	}
}
