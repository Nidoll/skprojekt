﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSystem : MonoBehaviour {

    private GameObject Trigger;
    private bool TriggerDestroy;

    public Text nameText;
    public Text dialogText;
    public Image CharakterImage;
    public PlayerController PlayerControllerO;

    public Animator animator;

    public Queue<string> sentences;
    private Queue<string> characternames;
    private Queue<AudioClip> SoundClips;

    private AudioSource AudioSourceDialog;

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(gameObject);

        sentences = new Queue<string>();
        characternames = new Queue<string>();
        SoundClips = new Queue<AudioClip>();

        PlayerControllerO = PlayerController.FindObjectOfType<PlayerController>();

        AudioSourceDialog = GetComponent<AudioSource>();
	}

    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.E))
        {

            DisplayNextSentence();

        }

    }

    public void StartDialog(Dialog dialog, GameObject trigger, bool triggerdestroy)
    {

        Trigger = trigger;
        TriggerDestroy = triggerdestroy;

        PlayerControllerO.SwitchDialogPhase();

        animator.SetBool("IsOpen", true);

        CharakterImage.sprite = dialog.CharakterImage;
        //nameText.text = dialog.name; 

        sentences.Clear();

        foreach (string sentence in dialog.sentences)
        {
            sentences.Enqueue(sentence);
        }

        foreach (string charactername in dialog.characternames)
        {
            characternames.Enqueue(charactername);
        }

        foreach (AudioClip SoundClip in dialog.Sounds)
        {
            SoundClips.Enqueue(SoundClip);
        }

        DisplayNextSentence();

    }
	
	public void DisplayNextSentence()
    {
        AudioSourceDialog.Stop();
        if (sentences.Count == 0)
        {
            EndDialog();
            return;
        }

        string sentence = sentences.Dequeue();
        string charactername = characternames.Dequeue();
        AudioClip SoundClip = SoundClips.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
        nameText.text = charactername;
        AudioSourceDialog.PlayOneShot(SoundClip);
    }

    IEnumerator TypeSentence (string sentence)
    {

        dialogText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogText.text += letter;
            yield return null;
        }

    }

    void EndDialog()
    {
        if (animator.GetBool("IsOpen") == true)
        {

            PlayerControllerO.SwitchDialogPhase();

        }

        animator.SetBool("IsOpen", false);


        if (TriggerDestroy == true)
        {

            Destroy(Trigger);

        }
    }

}
