﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vasen : MonoBehaviour {

	public GameObject[] Drops = new GameObject[2];
	private GameObject realDrop;
	private Vector3 Position;
	public Vector3 DropPosition;
	private Rigidbody2D rig;
	private Vector2 move;
	public float movespeed;

	void Start()
	{
		rig = GetComponent<Rigidbody2D>();
		int random;
		random = Random.Range(0,1);
		realDrop = Drops[random];
		Position = transform.position;
		DropPosition = new Vector3(Position.x + Random.Range(-1,1),Position.y + Random.Range(-1,1),Position.z);
	}
	void FixedUpdate()
	{
		//rig.MovePosition()
	}
	void OnTriggerEnter2D(Collider2D coll)
	{
		if(coll.CompareTag("Waffe"))
		{
			Break();
		}
	}

	void Break()
	{
		GetComponent<Animator>().SetBool("Break",true);
		Instantiate(realDrop,Position,transform.rotation);
		GetComponent<Collider2D>().enabled = false;
		Destroy(transform.Find("Shadow").gameObject);
	}
}
