﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {
	public GameObject[] Loot;
	public float[] Amount;
	private float x;
	private float y;
	private bool active;
	private bool looted = false;

	void Start()
	{

	}

	void Update()
	{
			if(Input.GetKeyDown(KeyCode.E) && active == true && looted == false)
			{
				StartCoroutine(open());
			}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player") && looted == false)
		{
			transform.Find("Canvas").gameObject.SetActive(true);
			active = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.CompareTag("Player") && looted == false)
		{
			transform.Find("Canvas").gameObject.SetActive(false);
			active = false;
		}
	}

	IEnumerator open()
	{
			transform.Find("Solid").GetComponent<BoxCollider2D>().enabled = false;
			Destroy(transform.Find("Canvas").gameObject);
			for(int i = 0; i < Loot.Length;i++)
			{
				for(float a = 0; a < Amount[i]; a++)
				{
					x = Random.Range(-1f,1f);
					y = Random.Range(-1f,1f);
					Instantiate(Loot[i],new Vector3(transform.position.x + x,transform.position.y + y,0),Quaternion.identity);
				}
			}
			looted = true;
			yield break;
	}
}
