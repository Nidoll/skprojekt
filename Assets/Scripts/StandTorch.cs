﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandTorch : MonoBehaviour {

	private bool On;
	private Light Light;
	public float blinkTime;
	private float Time1;
	public float RangeDifference;
	private float LightRange;
	private float Time2;
	private GameObject Player;
	public float currentdistance;
	public float maxdistance;
	void Start () {
		On = true;
		Light = GetComponentInChildren<Light>();
		Time1 = 0;
		LightRange = Light.range;
		Player = PlayerManager.instance.Player;
	}
	
	void Update () 
	{
		Blinking();
		OnOff();
	}

	void Blinking()
	{
		if(On == true)
		{
			if(Time1 <= blinkTime)
			{
				Time1 += Time.deltaTime;
				Light.range = LightRange;
			}
			else{
				if(Time2 <= blinkTime)
				{
					Time2 += Time.deltaTime;
					Light.range = LightRange - RangeDifference;
				}
				else{
					Time2 = 0;
					Time1 = 0;
				}
			}
		}
		if(On == false)
		{
			Light.range = 0;
		}
	}

	void OnOff()
	{
		currentdistance = Mathf.Sqrt(Mathf.Pow(Player.transform.position.x-transform.position.x,2)+Mathf.Pow(Player.transform.position.y-transform.position.y,2));
		if(currentdistance >= maxdistance)
		{
			On = false;
		}else{
			On = true;
		}
	}
}
