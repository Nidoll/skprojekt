﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Dialog {

    public string name;
    public Sprite CharakterImage;

    [TextArea(3, 10)]
    public string[] sentences;

    [TextArea(1, 1)]
    public string[] characternames;

    public AudioClip[] Sounds;

}
