﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu2 : MonoBehaviour {

	// Use this for initialization
	public bool PauseMenuOpen = false;
	public GameObject Pausemenu;

	void Update () {

	
		if (Input.GetKeyDown(KeyCode.Escape)) {


			//Ueberprueft ob das Pausemenu momentan geoeffnet ist
			if(PauseMenuOpen){
				Time.timeScale = 1; //zeit wird fortgesetzt
			}else{
				Time.timeScale = 0; //zeit wird angehalten
			}

			Pausemenu.SetActive(!PauseMenuOpen); // oeffnet die pausemenu grafik
			Cursor.visible = (!PauseMenuOpen); //absicherung sichtbarer cursor
			PlayerManager.instance.Player.GetComponent<PlayerController>().enabled = PauseMenuOpen; //spieler kann charakter nicht mehr steuern
			PauseMenuOpen=!PauseMenuOpen; // schließt bzw oeffnet das menu


		}		
	}
}
