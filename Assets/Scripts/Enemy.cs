﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed;
    public GameObject player;
    public GameObject Gegner;
    public GameObject Arrow;

    private Collision2D GegnerColl;
    private BoxCollider2D Box2D;
    private Rigidbody2D rig;
    public float Distance;
    public float DS;

    public float HP;

    public bool hit;
    private bool hitS;


     void Awake()
    {
        HP = 20;

        player = GameObject.Find("Player");
        Gegner = GameObject.Find("Enemy");
        Arrow  = GameObject.Find("Arrowf");

    }


    // Use this for initialization
    void Start ()
    {
        rig = GetComponent<Rigidbody2D>();
        hit = false;
        Box2D = GetComponent<BoxCollider2D>();
        GegnerColl = Box2D.GetComponent<Collision2D>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (HP <= 0)
        {
            Destroy(gameObject);
        }

    }

    void FixedUpdate()
    {

        Treffer();

    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Arrow" )
        {
            hit = true;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Sword")
        {
            hitS = true;
        }
        
    }

    void Treffer()
    {

        if (hit == false)
        {

            float z = Mathf.Atan2((player.transform.position.y - transform.position.y), (player.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;

            transform.eulerAngles = new Vector3(0, 0, z);

            transform.position = transform.position + transform.up * Time.deltaTime * speed;

        }

        if (hit == true)
        {

            HP = HP - 4;

            rig.MovePosition(transform.position - transform.up * Distance);

            hit = false;

        }

        if (hitS == true)
        {

            HP = HP - 5;

            rig.MovePosition(transform.position - transform.up * DS);

            hitS = false;

        }

    }

    
}
