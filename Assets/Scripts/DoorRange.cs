﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRange : MonoBehaviour {

	public bool passed;

    void Start()
    {
        passed = false;
    }
    void Update()
    {
        if(passed == true)
        {
            transform.parent.GetComponent<Door>().DissableDoor();
        }
    }

    public void DoorPassed()
    {
        passed = true;
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            passed = false;
            transform.parent.GetComponent<Door>().EnableDoor();
        }
    }
}
